import React, { useEffect, useState } from 'react'
import ProductList from '@components/ProductList/ProductList';
import { KawaiiHeader } from '@components/KawaiiHeader/KawaiiHeader';

import Layout from '@components/Layout/Layout';
import Link from 'next/link';

const HomePage = () => {
  const [productList, setProductList] = useState<TProduct[]>([]);

  useEffect(() => {
    fetch('/api/avo')
      .then(response => response.json())
      .then(({data}: TAPIAvoResponse) => {
        setProductList(data)
      })
  }, [])

  return (
    <Layout>
      <KawaiiHeader />
      <section>
        <Link href="/yes-or-no">
          <a>¿Deberia comer un avo hoy?</a>
        </Link>
      </section>
      <ProductList products={productList} />
      <style jsx>{`
        section {
          text-align: center;
          margin-bottom: 2rem;
        }
      `}</style>
    </Layout>
  )
}

export default HomePage
